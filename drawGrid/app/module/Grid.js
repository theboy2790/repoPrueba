export default class DrawGrid {
    constructor(width,height) {
        this.width=width;
        this.height=height;
    }

    drawGrid(numberCol,numberRow){
        var bufferGrid=document.createElement("canvas");
        var contextBuffer=bufferGrid.getContext('2d');

        bufferGrid.width=this.width;
        bufferGrid.height=this.height;
        
        var linesx=this.width/numberCol;
        var linesy=this.height/numberRow;

        for (var x = 0; x < numberCol; x++) {
            var widthCell=this.returnNumberPresition(linesx);
            contextBuffer.lineWidth=0.5;
            contextBuffer.beginPath();
            contextBuffer.moveTo(x*widthCell,0);
            contextBuffer.lineTo(x*widthCell,this.height);
            contextBuffer.strokeStyle='red';
            contextBuffer.stroke();
        }
    
        for (var y = 0; y < numberRow; y++) {
            var heightCell=this.returnNumberPresition(linesy);
            contextBuffer.beginPath();
            contextBuffer.moveTo(0,y*heightCell);
            contextBuffer.lineTo(this.width,y*heightCell);
            contextBuffer.stroke();
        }

        return bufferGrid;

    }
    returnNumberPresition(number) { 
        var positionDecimal=number.toString().indexOf(".");
        (positionDecimal!=-1)?number=number.toPrecision(positionDecimal+2):false;
        return number;
    }

}