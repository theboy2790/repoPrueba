import DrawGrid from "./module/Grid.js";

var canvas=document.getElementById("lienzo");
canvas.style.boxShadow="0px 0px 3px 0px black";
var context=canvas.getContext('2d');

var img=new Image();
img.src='resources/spritebirds.png';
img.onload=function () {  
    canvas.width=this.width;
    canvas.height=this.height;
    context.drawImage(this, 0, 0);
}

canvas.addEventListener("mousedown",function (e) {
    console.log(e);
});

canvas.addEventListener("mouseup",function (e) {
    console.log(e);
});



document.querySelector(".btnDrawGrid").addEventListener("click",function () {  

    context.clearRect(0, 0, canvas.width, canvas.height);
    var colNumber=document.getElementsByName("col")[0].value || 1;
    var rowNumber=document.getElementsByName("row")[0].value || 1;

    context.drawImage(img, 0, 0);

    var grid=new DrawGrid(canvas.width,canvas.height);
    context.drawImage(grid.drawGrid(colNumber,rowNumber),0,0);


});


